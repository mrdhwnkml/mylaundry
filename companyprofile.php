<!DOCTYPE html>
<?php session_start(); ?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>My Laundry</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="body">

  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="fa fa-envelope-o"></i> <a href="mailto:ridhwanm69@gmail.com">info@mylaundry.com</a>
        <i class="fa fa-phone"></i> 021-5525839
      </div>
    </div>
  </section>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.php" class="scrollto">My<span>Laundry</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="index.php">Home</a></li>
          <li><a href="contactus.php">Contact Us</a></li>
          <li class="menu-has-children"><a href="">About Us</a>
            <ul>
              <li><a href="companyprofile.php">Company Profile</a></li>
              <li><a href="so.php">Struktur Organisasi</a></li>
			    <li><a href="announcement.php">Announcement</a></li>
            </ul>
			 <li><a href="carrier.php">Carrier</a></li>
		  <li><a>
			<?php
			//session_start();
			if(isset($_SESSION['nama'])) { ?>
			<li><a <?php //echo 'Selamat Datang, '; ?> </a></li>
			<li><?php
			echo 'Selamat Datang, ';
			echo $_SESSION['nama'];
			?></li>
		  </a></li>	
			<li class="menu-has-children"><a href="form-login/logout.php">Logout</a></li>
			<?php
			} else {
			?> 
			<li class="menu-has-children"><a href="">Account</a>
				<ul>
				  <li><a href="form-login/login.php">Login</a></li>
				  <li><a href="form-login/register.php">Register</a></li>
				</ul>			
			</li>
			
			<?php
			//echo 'Selamat Datang, ';
			//echo   $_SESSION['nama'];
			} ?>
			
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
 

    <!--==========================
      About Section
    ============================-->
   <!-- #about -->

    <!--==========================
      Services Section
    ============================-->
   <!-- #services -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- #clients -->

    <!--==========================
      Our Portfolio Section
    ============================-->
    <!-- #portfolio -->

    <!--==========================
      Testimonials Section
    ============================-->
    <!-- #testimonials -->

    <!--==========================
      Call To Action Section
    ============================-->
    <!-- #call-to-action -->

    <!--==========================
      Our Team Section
    ============================-->
     
<!-- #team -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Company Profile</h2>
        </div>
		<center><img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                "Selama orang masih memakai tekstil dan produk tekstil
		maka selama itu pula orang masih perlu usaha jasa Laundry"
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt=""></center>
		<p></p>
        <span align="justify">
		<font size="6">MyLaundry</font> didirikan pada tanggal 5 Agustus 2016. Berlokasi di  Jl. Akses UI Cimanggis, DepokRaya. 
	    MyLaundry adalah perusahaan yang bergerak dibidang jasa Laundry, dan penjualan penyediaan Laundry Equiptment 
		untuk kebutuhan: Rumah Sakit, Hotel, Industri dan Pribadi.</span>
		<p>
		<h5><center>VISI</center></h5>
		<center>Menjadi model perusahaan kuat berbasis ENTREPRENEURSHIP, berorientasi pada kepuasan pelanggan serta <br/>
		menghasilkan Sumber Daya Manusia yang handal dengan Implementasi Manajemen Modern.</center>
		<p>
		<h5><center>MISI</center></h5>
		<center>Mendirikan, Menjalankan, Mengembangkan Usaha Laundry & Drycleaning di Seluruh Indonesia.</center>
		<p>
		<h5><center>TUJUAN PERUSAHAAN</center></h5>


<ul>
<li>Melalui Franchise diharapkan dapat menjadi sebuah usaha jasa Laundry yang berkembang di Indonesia.</li>
<li>Memiliki metode dalam usaha jasa Laundry yang dapat dijadikan acuan.&nbsp;</li>
<li>Menjadikan jasa Laundry sebagai bagian dalam kehidupan keluarga.&nbsp;</li>
<li>Dengan motto We Serve You Better kami akan menjadi yang terdepan di bidang Laundry.</li>
</ul>
<p>
		<h5><center>PELUANG PASAR</center></h5>
<ul>
<li>Peluang pasar sangat luas dengan bermunculnya trend-trend baju eksklusif yang perlu perawatan khusus.</li>
<li>Populasi dimana dunia tekstil, fashion, dan mode berkembang dengan pesat.</li>
<li>Populasi masyarakat golongan sosial ekonomi menengah dan atas, yang peduli akan kebersihan, kerapian, dan penampilan semakin besar.</li>
<p>
<h5><center>KELEBIHAN</center></h5>
<li>Memiliki Visi dan Misi kedepan yang sangat jelas.</li>
<li>Telah lama berpengalaman mengelola usaha jasa Laundry.</li>
<li>Menggunakan sistem Franchise dengan manajemen yang telah teruji baik.</li>
<li>Menggunakan System IT untuk proses transaksi dan pelaporan.</li>
<li>Konsultan Marketing bekerja sama dengan BenWarG Consulting Jakarta.</li>
<li>Proses Produksi/Pencucian bekerja sama dengan Ecolab USA cabang Indonesia.</li>
		
		</div>
      </section>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>MyLaundry</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Reveal
        -->
        <a href="https://bootstrapmade.com/"></a> 
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
