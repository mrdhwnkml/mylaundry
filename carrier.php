<!DOCTYPE html>
<?php session_start(); ?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>My Laundry</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="body">

  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="fa fa-envelope-o"></i> <a href="mailto:ridhwanm69@gmail.com">info@mylaundry.com</a>
        <i class="fa fa-phone"></i> 021-5525839
      </div>
    </div>
  </section>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.php" class="scrollto">My<span>Laundry</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="index.php">Home</a></li>
          <li><a href="contactus.php">Contact Us</a></li>
          <li class="menu-has-children"><a href="">About Us</a>
            <ul>
              <li><a href="companyprofile.php">Company Profile</a></li>
              <li><a href="#">Struktur Organisasi</a></li>
			    <li><a href="announcement.php">Announcement</a></li>
            </ul>
			 <li><a href="carrier.php">Carrier</a></li>
		  <li><a>
			<?php
			//session_start();
			if(isset($_SESSION['nama'])) { ?>
			<li><a <?php //echo 'Selamat Datang, '; ?> </a></li>
			<li><?php
			echo 'Selamat Datang, ';
			echo $_SESSION['nama'];
			?></li>
		  </a></li>	
			<li class="menu-has-children"><a href="form-login/logout.php">Logout</a></li>
			<?php
			} else {
			?> 
			<li class="menu-has-children"><a href="">Account</a>
				<ul>
				  <li><a href="form-login/login.php">Login</a></li>
				  <li><a href="form-login/register.php">Register</a></li>
				</ul>			
			</li>
			
			<?php
			//echo 'Selamat Datang, ';
			//echo   $_SESSION['nama'];
			} ?>
			
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
 

    <!--==========================
      About Section
    ============================-->
   <!-- #about -->

    <!--==========================
      Services Section
    ============================-->
   <!-- #services -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- #clients -->

    <!--==========================
      Our Portfolio Section
    ============================-->
    <!-- #portfolio -->

    <!--==========================
      Testimonials Section
    ============================-->
    <!-- #testimonials -->

    <!--==========================
      Call To Action Section
    ============================-->
    <!-- #call-to-action -->

    <!--==========================
      Our Team Section
    ============================-->
     
<!-- #team -->

    <!--==========================
      Contact Section
    ============================-->
	<form action="apply.php" method="get">
    <section id="contact" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Carrier</h2>
        </div>

       <font size="6">MyLaundry</font> Perusahaan yang bergerak dalam bidang jasa laundry dan franchise laundry terbesar di Indonesia, 
	   dengan jaringan tersebar di seluruh wilayah Indonesia membuka kesempatan untuk bergabung bersama kami, posisi:
		<p></p>
	   <b>IT SUPPORT</b>
	   <p></p>
	   Kualifikasi :
<p>
<ol>
<li>D3 / S1 bidang Ilmu Komputer.</li>
<li>Mahir Windows System, Linux System, Networking, Troubleshooting.</li>
<li>Mampu bekerja dalam individu / tim.</li>
<li>Memiliki motivasi kerja yang tinggi, energik, dan kreatif.</li>
<li>Ulet dan pekerja keras.</li>
<li>Bertanggung jawab terhadap pekerjaan.</li>
<li>Menguasai bahasa pemrograman AS/400 atau IT product development dan networking komunikasi data atai metodologi pengembangan aplikasi (SDLC, waterfall) dan project management.</li>
</ol>
Job Deskripsi :
<p>
<ol>
<li>Menerima, memprioritaskan dan menyelesaikan permintaan bantuan IT.</li>
<li>Membeli hardware IT, software dan hal-hal lain yang berhubungan dengan hal tersebut.</li>
<li>Instalasi, perawatan dan penyediaan dukungan harian baik untuk hardware & software Windows & Macintosh, peralatan termasuk printer, scanner, hard-drives external, dll.</li>
<li>Korespondensi dengan penyedia jasa eksternal termasuk Internet Service Provider, penyedia jasa Email, hardware, dan software supplier, dll.</li>
<li>Mengatur penawaran harga barang dan tanda terima dengan supplier untuk kebutuhan yang berhubungan dengan IT.</li>
<li>Menyediakan data / informasi yang dibutuhkan untuk pembuatan laporan department regular.</li>
</ol>
<a href=apply.php?posisi=IT  class="btn btn-success">Apply</a>  
<p>
<p>
<b>ACCOUNTING</b>
<p>
Kualifikasi :
<p>
<ol>
<li>Usia maksimal 40 tahun.</li>
<li>S1 Ekonomi, Akuntansi, Perpajakan.</li>
<li>IPK minimal 2,75 (skala 4,00).</li>
<li>Memahami prinsip dasar­dasar Akuntansi dan Pajak.</li>
<li>Menguasai proses kerja keuangan, Analisis Keuangan, dan lain­lain.</li>
<li>Memiliki pengalaman di bidang Akutansi menjadi prioritas.</li>
<li>Memiliki pemahaman tentang aplikasi Software Akuntansi Zahir, Accurate lebihdiutamakan.</li>
<li>Memiliki kemampuan dalam menggunakan software komputer seperti Microsoft Office (Word, Excel, PowerPoint).</li>
</ol>
 <p>
Job Deskripsi :
<p>
<ol>
<li>Mengelola Akuntansi dan Departemen Keuangan.</li>
<li>Melakukan proses kerja keuangan.</li>
<li>Membuat laporan keuangan.</li>
</ol>
<a href=apply.php?posisi=Akuntansi class="btn btn-success">Apply</a>

<p>
<p>
<b>CUSTOMER SERVICE</b>
<p>
Kualifikasi :
<p>
<ol>
<li>Memiliki pengalaman dibidangnya minimal 1 tahun.</li>
<li>Pendidikan minimal SLTA/sederajat.</li>
<li>Memiliki kemampuan komunikasi yang baik.</li>
<li>Dapat bekerja dibawah tekanan.</li>
<li>Jujur, bertanggung jawab, ramah, sopan, aktif, memiliki motivasi tinggi dan wawasan yang luas.</li>
<li>Dapat menggunakan Ms. Word, Excel.</li>
<li>Bersedia bekerja shifting dan masuk pada hari libur nasional.</li>
</ol>
<p>
Job Deskripsi :
<p>
<ol>
<li>Membangun hubungan saling percaya yang berkelanjutan melalui komunikasi yang terbuka dam interaktif.</li>
<li>Mampu multi fungsi dan mengelola waktu secara efektif.</li>
<li>Memberikan informasi yang dibutuhkan customer.</li>
</ol>
<a href=apply.php?posisi=CS class="btn btn-success">Apply</a>
	</div>

      </section>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>MyLaundry</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Reveal
        -->
        <a href="https://bootstrapmade.com/"></a> 
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
