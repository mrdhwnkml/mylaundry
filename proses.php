<!DOCTYPE html>
<?php session_start(); ?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>My Laundry</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="body">

  <!--==========================
    Top Bar
  ============================-->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="fa fa-envelope-o"></i> <a href="mailto:ridhwanm69@gmail.com">info@mylaundry.com</a>
        <i class="fa fa-phone"></i> 021-5525839
      </div>
    </div>
  </section>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.php" class="scrollto">My<span>Laundry</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="index.php">Home</a></li>
          <li><a href="contactus.php">Contact Us</a></li>
          <li class="menu-has-children"><a href="">About Us</a>
            <ul>
              <li><a href="companyprofile.php">Company Profile</a></li>
              <li><a href="#">Struktur Organisasi</a></li>
            </ul>
			 <li><a href="carrier.php">Carrier</a></li>
		  <li><a>
			<?php
			//session_start();
			if(isset($_SESSION['nama'])) { ?>
			<li><a <?php //echo 'Selamat Datang, '; ?> </a></li>
			<li><?php
			echo 'Selamat Datang, ';
			echo $_SESSION['nama'];
			?></li>
		  </a></li>	
			<li class="menu-has-children"><a href="form-login/logout.php">Logout</a></li>
			<?php
			} else {
			?> 
			<li class="menu-has-children"><a href="">Account</a>
				<ul>
				  <li><a href="form-login/login.php">Login</a></li>
				  <li><a href="form-login/register.php">Register</a></li>
				</ul>			
			</li>
			
			<?php
			//echo 'Selamat Datang, ';
			//echo   $_SESSION['nama'];
			} ?>
			
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
 

    <!--==========================
      About Section
    ============================-->
   <!-- #about -->

    <!--==========================
      Services Section
    ============================-->
   <!-- #services -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- #clients -->

    <!--==========================
      Our Portfolio Section
    ============================-->
    <!-- #portfolio -->

    <!--==========================
      Testimonials Section
    ============================-->
    <!-- #testimonials -->

    <!--==========================
      Call To Action Section
    ============================-->
    <!-- #call-to-action -->

    <!--==========================
      Our Team Section
    ============================-->

	 
	 
	 
	 
   <?php
  
	if(isset($_GET['Submit'])){
		include 'koneksi.php';
		//$aku = mysqli_query($conn, "insert into datakaryawan values (
		
		
		$nama = $_GET['nama'];
		$tempatlhr = $_GET['tempatlhr'];
		$tanggallhr = $_GET['tanggallhr'];
		$telepon = $_GET['telepon'];
		$email = $_GET['email'];
		$posisi = $_GET['posisi'];

		
		$input = mysql_query("insert into datakaryawan values (NULL, '$nama', '$tempatlhr', '$tanggallhr',
		'$telepon','$email','$posisi')") or die (mysql_error());
	
	
    if($input){ ?>
	<br>
<h2><center>Selamat, data anda telah terkirim.</center></h2> <?php
	} else {
	
		  echo 'Gagal Daftar, silahkan coba lagi.' ;
	}
	}
	?>
    
	
	<div class="container">	
	<table class='table'>
	<tr>
	<td>Nama Lengkap</td><td><?php echo $nama = $_GET['nama'] ?></td>
	</tr>
	<tr>
	<td>Tempat Lahir<td><?php echo $nama = $_GET['tempatlhr'] ?></td>
	</tr>
	<tr>
	<td>Tanggal Lahir</td><td><?php echo $nama = $_GET['tanggallhr'] ?></td>
	</tr>
	<tr>
	<td>Telepon</td><td><?php echo $nama = $_GET['telepon'] ?></td>
	</tr>
	<tr>
	<td>Email</td><td><?php echo $nama = $_GET['email'] ?></td>
	</tr>
	<tr>
	<td>Posisi</td><td><?php echo $nama = $_GET['posisi'] ?></td>
	</tr>
	</div>
	</table>
	 <div class="container">		
	<p>
	<form action ="upload.php" method="post" enctype="multipart/form-data">
	<h5><u>III. Unggah CV (Upload Curicculum Vitae)</u></h5> 
	<h6>*Format nama gambar : Nama_posisi. Contoh : Bambang_IT<h6/>
	
		<tr>
		<td>Pilih File : <input type="file" name="file[]" /></td>
		</tr>		
		</br> 
		</br>
	
	<h5><u>III. Unggah Foto (Upload Photo)</u></h5> 
	<h6>*Format nama gambar : Nama_posisi. Contoh : Bambang_IT<h6/>
	
		<tr>
		<td>Pilih File : <input type="file" name="file[]" /></td>
		
		</tr>
		</br>
		</br>
		</br>
		</div>
		
		<tr>
		<input type="submit" name="Submit" value="Upload" />
		</tr>
</form>
	</br>
	</br>
		
  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>MyLaundry</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Reveal
        -->
        <a href="https://bootstrapmade.com/"></a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/sticky/sticky.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
